

// 8 output of the multiplexer, channel select pins
const int MULTI = 8;
const int BIT0_PIN = 7;
const int BIT1_PIN = 8;
const int BIT2_PIN = 9;
int bit0 = 0;
int bit1 = 0;
int bit2 = 0;

// MIDI settings
const int STATUS_BYTE = 176;
const int CONTROL_VALUES[] = {20, 21, 22, 23, 24, 25, 26, 27 }; 
int potiValues[MULTI];
int potiReads[MULTI];


int i = 0;

void setup() {
  pinMode(BIT0_PIN, OUTPUT);
  pinMode(BIT1_PIN, OUTPUT);
  pinMode(BIT2_PIN, OUTPUT);
  Serial.begin(9600);
}

void loop () {
  for (i = 0; i < MULTI; i++) {
    bit0 = bitRead(i, 0);
    bit1 = bitRead(i, 1);
    bit2 = bitRead(i, 2);

    digitalWrite(BIT0_PIN, bit0);
    digitalWrite(BIT1_PIN, bit1);
    digitalWrite(BIT2_PIN, bit2);

    potiReads[i] = map(analogRead(A0), 0, 1023, 0, 127);
    if (potiReads[i] != potiValues[i]) {
      Serial.write(STATUS_BYTE);
      Serial.write(CONTROL_VALUES[i]);
      Serial.write(potiReads[i]);
      potiValues[i] = potiReads[i];
    }
  }
}

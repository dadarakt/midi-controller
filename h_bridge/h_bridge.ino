#include <CapacitiveSensor.h>

// H2 bridge control pins
const int H1 = 10;
const int H2 = 11;

// the fader values
const int FADER_PIN = A0;
const int FADER_TOLERANCE = 10;
int faderRead = 0;
int faderValue = 0;
int currMidi = 0;
int newMidi = 0;


// poti to set the desired slider value
const int TARGET_PIN = A1;
int targetValue = 0;
int potiValue = 0;
int potiRead = 0;

const int UPDATE_FACTOR = 0.5;

// Capacitive touch sensor pins
CapacitiveSensor cap = CapacitiveSensor(12, 13);
bool touched = false;
const int TOUCH_THRESH = 100;

void setup() {
  pinMode(H1, OUTPUT);
  pinMode(H2, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  potiRead = 0.4 * analogRead(TARGET_PIN) + 0.6 * potiValue;
  if (potiRead != potiValue) {
    targetValue = potiRead;
    potiValue = potiRead;
  }
  faderRead = 0.3 * analogRead(FADER_PIN) + 0.7 * faderValue;
  if (faderRead != faderValue) {
    Serial.write(176);
    Serial.write(21);
    Serial.write(map(faderRead, 0, 1023, 0, 127));
    faderValue = faderRead;
  }
  
  touched = cap.capacitiveSensor(30)> TOUCH_THRESH;
   
  if (!touched) {
    if (faderValue > targetValue + 30) {
      motorDown();
    } else if (faderValue < targetValue -30) {
      motorUp();
    } else {
      motorStop();
    }
  } else {
    targetValue = faderValue;
    motorOff();
  }
}

void motorDown() {
  digitalWrite(H1, HIGH);
  digitalWrite(H2, LOW);
}

void motorUp() {
  digitalWrite(H1, LOW);
  digitalWrite(H2, HIGH);
}

void motorStop() {
  digitalWrite(H1, HIGH);
  digitalWrite(H2, HIGH);
}

void motorOff() {
  digitalWrite(H1, LOW);
  digitalWrite(H2, LOW);
}

int updateValue(int newValue, int oldValue, float updateWeight) {
  return newValue * updateWeight + oldValue * (1 - updateWeight);
}


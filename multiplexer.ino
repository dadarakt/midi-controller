int potiWert = 0;
int zaehler = 0;

// 8 output of the multiplexer
const int MULTI = 8;
int bit0 = 0;
int bit1 = 0;
int bit2 = 0;

const int BIT0_PIN = 7;
const int BIT1_PIN = 8;
const int BIT2_PIN = 9;

void setup() {
  pinMode(BIT0_PIN, OUTPUT);
  pinMode(BIT1_PIN, OUTPUT);
  pinMode(BIT2_PIN, OUTPUT);
  pinMode(A0, INPUT):
  Serial.begin(9600);
}

void loop () {
  for (i = 0; i < MULTI; i++) {
    bit0 = bitRead(i, 0);
    bit1 = bitRead(i, 1);
    bit2 = bitRead(i, 2);

    digitalWrite(BIT0_PIN, bit0);
    digitalWrite(BIT1_PIN, bit1);
    digitalWrite(BIT2_PIN, bit2);
    
    potiWert = analogRead(A0);
    Serial.print(potiWert);
    Serial.print("\t");
  }
  Serial.println("");
}


const int DATA_PIN = 11;
const int CLOCK_PIN = 12;
const int LATCH_PIN = 8;

const int poti = A0;

int pos;
int byteIdx;
int currState;
byte chosenByte;
int i;

int del;
int potRead;

const int GREEN = 0;
const int RED = 1;

const int NUM_VALUES = 2;
byte values[NUM_VALUES];

void setup() {
  Serial.begin(9600);  
  pinMode(DATA_PIN, OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);
  pinMode(LATCH_PIN, OUTPUT);

  for (int j = 0; j < 4; j++) {
    for (i = 0; i < NUM_VALUES; i++) {
      values[i] = 0;
    }
    writeValues();
    delay(200);
    for (i = 0; i < NUM_VALUES; i++) {
      values[i] = 255;
    }
    writeValues();
    delay(200);
  }
}

void loop() {
//  if (Serial.available() > 0) {
//    pos = Serial.read() - 48;
//    if (pos < 8 && pos >= 0) {
//      Serial.print("toggling position: ");
//      Serial.println(pos);
//      toggleValue(pos);
//    }
//  }  
  
  for (int k = 0; k < NUM_VALUES*8 ; k++) {
    potRead = analogRead(poti);
    del = map(potRead, 0, 1023, 0, 250);
    toggleValue(k);
    delay(del);
  }
}


void toggleValue(int idx) {
  byteIdx = idx / 8;
  chosenByte = values[byteIdx];
  currState = bitRead(chosenByte, idx % 8);
  if (currState == 1) {
    bitWrite(chosenByte, idx % 8, 0);
  } else {
    bitWrite(chosenByte, idx % 8, 1);
  }
  values[byteIdx] = chosenByte;
  writeValues();
}

void writeValues() {
  digitalWrite(LATCH_PIN, LOW);
  for (i = NUM_VALUES - 1; i >= 0; i--) {
    shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, values[i]);
  }
  digitalWrite(LATCH_PIN, HIGH);
}


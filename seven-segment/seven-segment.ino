const int pins[8] = {2, 3, 4, 5, 7, 8, 9};
const int dotPin = 6;
const int nums = 10;
const byte numbers[nums][7] = {
  {1, 0, 0, 0, 0, 0, 0},
  {1, 1, 1, 0, 0, 1, 1},
  {0, 1, 0, 0, 1, 0, 0},
  {0, 1, 0, 0, 0, 0, 1},
  {0, 0, 1, 0, 0, 1, 1},
  {0, 0, 0, 1, 0, 0, 1},
  {0, 0, 0, 1, 0, 0, 0},
  {1, 1, 0, 0, 0, 1, 1},
  {0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 1}
};


int i = 0;
int numI = 0;

void setup() {
  for (i = 0; i < 8; i++) {
    pinMode(pins[i], OUTPUT);
  }
  pinMode(dotPin, OUTPUT);
}

void loop() {
 for (i = 0; i < nums; i ++) {
   writeNumber(i, false);
   delay(250);
   writeNumber(i, true);
   delay(250);
 }
}
  

void writeNumber(byte num, boolean dot) {
  if (dot) {
    digitalWrite(dotPin, HIGH);
  } else {
    digitalWrite(dotPin, LOW);
  }
  for (numI = 0; numI < 7; numI++) {
    digitalWrite(pins[numI], numbers[num][numI]);
  }
}


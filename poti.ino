#include <Bounce2.h>

// Pins
const int BUTTON1 = 2;
const int BUTTON2 = 4;
const int potiPin = A0;

// MIDI constants
const int statusByte = 176; // CC on channel 1
const int controllerNum = 21;
int controllerValue = 0;

const int noteStatusByte = 144; // note on channel 1
const int note = 36; // note C1
const int noteValue = 127;

int buttonState = LOW;
int buttonRead = LOW;

Bounce debouncer1 = Bounce();
Bounce debouncer2 = Bounce();

int potiRead = 0;

void setup() {
  debouncer1.attach(BUTTON1, INPUT_PULLUP);
  debouncer2.attach(BUTTON2, INPUT_PULLUP);
  debouncer1.interval(25);
  debouncer2.interval(25);
  pinMode(potiPin, INPUT);
  Serial.begin(9600);
}

void loop() {  
  debouncer1.update();
  debouncer2.update();
  
  potiRead = map(analogRead(potiPin),1, 1024, 0, 127);

  if (potiRead < controllerValue - 1 or potiRead > controllerValue + 1 ) {
    Serial.write(statusByte);
    Serial.write(controllerNum);
    Serial.write(controllerValue);
    controllerValue = potiRead;
  }

  if (debouncer1.rose()) {
    Serial.write(144);
    Serial.write(36);
    Serial.write(127);
  } 

  if(debouncer2.rose()) {
    Serial.write(144);
    Serial.write(36);
    Serial.write(127);
  }
  
}
